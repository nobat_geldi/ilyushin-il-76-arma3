class CfgFactionClasses
{
	class CIV_DE {
		displayName = "$STR_DE";
		priority = 8;
		side = TCivilian;
	};
	class CIV_SU {
		displayName = "$STR_SU";
		priority = 8;
		side = TCivilian;
	};
	class CIV_UA {
		displayName = "$STR_UA";
		priority = 8; 
		side = TCivilian;
	};
	class CIV_LI {
		displayName = "$STR_LI";
		priority = 8;
		side = TCivilian;
	};
	class CIV_LA {
		displayName = "$STR_LA";
		priority = 8;
		side = TCivilian;
	};
	class CIV_CU {
		displayName = "$STR_CU";
		priority = 8;
		side = TCivilian;
	};
	class CIV_GE {
		displayName = "$STR_GE";
		priority = 8;
		side = TCivilian;
	};
	class CIV_AM {
		displayName = "$STR_AM";
		priority = 8;
		side = TCivilian;
	};
	class CIV_KZ {
		displayName = "$STR_KZ";
		priority = 8;
		side = TCivilian;
	};
	class CIV_GB {
		displayName = "$STR_GB";
		priority = 8;
		side = TCivilian;
	};
	class CIV_NZ {
		displayName = "$STR_NZ";
		priority = 8;
		side = TCivilian;
	};
	class CIV_AR {
		displayName = "$STR_AR";
		priority = 8;
		side = TCivilian;
	};
	class CIV_AU {
		displayName = "$STR_AU";
		priority = 8;
		side = TCivilian;
	};
	class CIV_NB {
		displayName = "$STR_NB";
		priority = 8;
		side = TCivilian;
	};
	class CIV_CA {
		displayName = "$STR_CA";
		priority = 8;
		side = TCivilian;
	};
	class CIV_FR {
		displayName = "$STR_FR";
		priority = 8;
		side = TCivilian;
	};
	class CIV_TW {
		displayName = "$STR_TW";
		priority = 8;
		side = TCivilian;
	};
	class CIV_US {
		displayName = "$STR_US";
		priority = 8;
		side = TCivilian;
	};
	class CIV_JP {
		displayName = "$STR_JP";
		priority = 8;
		side = TCivilian;
	};
	class CIV_CH {
		displayName = "$STR_CH";
		priority = 8;
		side = TCivilian;
	};
	class CIV_FJ {
		displayName = "$STR_FJ";
		priority = 8;
		side = TCivilian;
	};
	class CIV_BR {
		displayName = "$STR_BR";
		priority = 8;
		side = TCivilian;
	};
	class CIV_LX {
		displayName = "$STR_LX";
		priority = 8;
		side = TCivilian;
	};
	class CIV_NE {
		displayName = "$STR_NE";
		priority = 8;
		side = TCivilian;
	};
	class CIV_NR {
		displayName = "$STR_NR";
		priority = 8;
		side = TCivilian;
	};
	class CIV_SW {
		displayName = "$STR_SW";
		priority = 8;
		side = TCivilian;
	};
	class CIV_CN {
		displayName = "$STR_CN";
		priority = 8;
		side = TCivilian;
	};


	class CIV_DD {
		displayName = "$STR_DD";
		priority = 8;
		side = TCivilian;
	};
	class CIV_CZ {
		displayName = "$STR_CZ";
		priority = 8;
		side = TCivilian;
	};
	class CIV_PL {
		displayName = "$STR_PL";
		priority = 8;
		side = TCivilian;
	};
	class CIV_UZ {
		displayName = "$STR_UZ";
		priority = 8;
		side = TCivilian;
	};
	class CIV_KG {
		displayName = "$STR_KG";
		priority = 8;
		side = TCivilian;
	};
	class CIV_HU {
		displayName = "$STR_HU";
		priority = 8;
		side = TCivilian;
	};
	class CIV_BU {
		displayName = "$STR_BU";
		priority = 8;
		side = TCivilian;
	};
	class CIV_BL {
		displayName = "$STR_BL";
		priority = 8;
		side = TCivilian;
	};
	class CIV_IR {
		displayName = "$STR_IR";
		priority = 8;
		side = TCivilian;
	};
	class CIV_SY {
		displayName = "$STR_SY";
		priority = 8;
		side = TCivilian;
	};
	class CIV_SK {
		displayName = "$STR_SK";
		priority = 8;
		side = TCivilian;
	};
	class CIV_KR {
		displayName = "$STR_KR";
		priority = 8;
		side = TCivilian;
	};
	class CIV_IN {
		displayName = "$STR_IN";
		priority = 8;
		side = TCivilian;
	};
	class CIV_UN {
		displayName = "$STR_UN";
		priority = 8;
		side = TCivilian;
	};
	class CIV_IQ {
		displayName = "$STR_IQ";
		priority = 8;
		side = TCivilian;
	};
};
